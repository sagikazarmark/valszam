package main

import (
	"os"
	"encoding/csv"
	"io"
	"strconv"
	"fmt"
)

type Huzas struct {
	Ev     int
	Het    int
	Nap    int
	Datum  string
	Szamok []int
}

func main() {
	f, err := os.Open("keno.csv")
	if err != nil {
		panic(err)
	}

	reader := csv.NewReader(f)
	reader.Comma = ';'
	reader.ReuseRecord = true
	reader.FieldsPerRecord = 24

	var huzasok []*Huzas

	for {
		aktualisHuzas, err := reader.Read()
		if err == io.EOF {
			break
		}

		ev, err := strconv.Atoi(aktualisHuzas[0])
		if err != nil {
			panic(err)
		}

		het, err := strconv.Atoi(aktualisHuzas[1])
		if err != nil {
			panic(err)
		}

		nap := 0
		if aktualisHuzas[2] != "" {
			var err error
			nap, err = strconv.Atoi(aktualisHuzas[2])
			if err != nil {
				panic(err)
			}
		}

		huzas := &Huzas{
			Ev:    ev,
			Het:   het,
			Nap:   nap,
			Datum: aktualisHuzas[3],
		}

		for i := 4; i < 24; i++ {
			szam, err := strconv.Atoi(aktualisHuzas[i])
			if err != nil {
				fmt.Println(len(huzasok))
				panic(err)
			}

			huzas.Szamok = append(huzas.Szamok, szam)
		}

		huzasok = append(huzasok, huzas)
	}

	huzasokSzama := len(huzasok)

	fmt.Printf("Huzasok szama: %d\n\n", huzasokSzama)


	// 1.A feladat: "legkisebb nyerőszám 10-es" relatív gyakorisága
	legkisebbTizesDarab := 0
	for _, huzas := range huzasok {
		// Kihasználjuk, hogy a számok növekvő sorrendben vannak
		if huzas.Szamok[0] == 10 {
			legkisebbTizesDarab++
		}
	}

	fmt.Printf("1.A (legkisebb nyerőszám 10-es relatív gyakorisága): %f\n", float64(legkisebbTizesDarab)/float64(huzasokSzama))

	// 1.B feladat: "1,2,3,4,5 számokat megjátszva pontosan három találat" relatív gyakorisága
	// 1.C feladat: "1,2,3,4,5 számokat megjátszva legalább három találat" relatív gyakorisága
	pontosanHaromTalalatDarab := 0
	legalabbHaromTalalatDarab := 0
	megtettSzamok := []int{1, 2, 3, 4, 5}
	for _, huzas := range huzasok {
		// Eltalált számok száma az adott húzásból
		talalatokDarab := 0

		for _, huzottSzam := range huzas.Szamok {
			for _, megtettSzam := range megtettSzamok {
				if huzottSzam == megtettSzam {
					talalatokDarab++
					break
				} else if huzottSzam < megtettSzam { // Kihasználjuk, hogy a számok növekvő sorrendben vannak
					break
				}
			}
		}

		if talalatokDarab >= 3{
			 legalabbHaromTalalatDarab++

			if talalatokDarab == 3 {
				pontosanHaromTalalatDarab++
			}
		}
	}

	fmt.Printf("1.B (1,2,3,4,5 számokat megjátszva pontosan három találat relatív gyakorisága): %f\n", float64(pontosanHaromTalalatDarab)/float64(huzasokSzama))
	fmt.Printf("1.C (1,2,3,4,5 számokat megjátszva legalább három találat relatív gyakorisága): %f\n", float64(legalabbHaromTalalatDarab)/float64(huzasokSzama))
}
